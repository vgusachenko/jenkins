# usage
create your own seedjob, 

example: jobs/seed-job.groovy ( /usr/share/jenkins/ref/init.groovy.d/seed_job.groovy )


create your own jenkins yaml config, 

example: data/jenkins-config-as-code.yaml ( /usr/local/jenkins-config-as-code.yaml )

# build
```bash
git clone https://gitlab.com/vgusachenko/jenkins.git
docker build --rm -t registry.gitlab.com/vgusachenko/jenkins/jenkins:latest .
```

# run
```bash
docker run -d -p 8080:8080 --name jenkins -e ADMIN_PASSWORD=password registry.gitlab.com/vgusachenko/jenkins/jenkins:latest
```

# docker-compose example:
```yaml
version: "3"

services:

  jenkins:
    image: registry.gitlab.com/vgusachenko/jenkins/jenkins:latest
    container_name: jenkins
    ports:
    - 8080:8080
    restart: unless-stopped
    volumes:
    - ./jenkins/seed_job.groovy:/usr/share/jenkins/ref/init.groovy.d/seed_job.groovy
    - ./jenkins/jenkins-config-as-code.yaml:/usr/local/jenkins-config-as-code.yaml
    environment:
    - ADMIN_USER=admin
    - ADMIN_PASSWORD=password
    - CASC_JENKINS_CONFIG=/usr/local/jenkins-config-as-code.yaml
```

# jenkins-config-as-code.yaml samples:
```yaml
jenkins:
  securityRealm:
    local:
      allowsSignup: false
      users:
        - id: ${ADMIN_USER}
          password: ${ADMIN_PASSWORD}
  authorizationStrategy: loggedInUsersCanDoAnything
```

```yaml
credentials:
  system:
    domainCredentials:
    - credentials:
      - basicSSHUserPrivateKey:
          id: "jenkins"
          privateKeySource:
            directEntry:
              privateKey: |
                -----BEGIN OPENSSH PRIVATE KEY-----
                m9uZQAAAAAAAAABAAABlwAAAAdzc2gtcnb3BlbnNzaC1rZXktdjEAAAAABG5vbmUAAAAEb
                NhAAAAAwEAqudzVFImfr/B588VnVb1eyWMRllAQAAAYEAvtYmJ90zLbMx6iTnwmXBlOzhJ
                ......................................................................
                ZnKmYR7b0T9cAnCLa2Qkz19Uk8WmFnXIat6n/ERsA2TC8D1MNZGqL+5w7Pi/x+LgWYyRvv
                D0NGgQetWVepa25M6ChsDC/2J3BCO1bz/9T5VkQbuSfGFrdzi5Mm+2Dp0CkbMmal5/gjRC
                dooph6ahd0quiutuapaethei6ioSh5vaiThi0mai4eva==
                -----END OPENSSH PRIVATE KEY-----
          scope: GLOBAL
          username: "jenkins"

```

```yaml
jenkins:
  nodes:
  - permanent:
      labelString: "docker"
      launcher:
        ssh:
          credentialsId: "jenkins"
          host: "11.22.33.44"
          port: 22
          sshHostKeyVerificationStrategy:
            manuallyTrustedKeyVerificationStrategy:
              requireInitialManualTrust: false
      name: "jenkins_slave01"
      remoteFS: "/home/jenkins"
      retentionStrategy: "always"

```