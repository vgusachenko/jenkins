import jenkins.model.*

def matchedJobs = Jenkins.instance.items.findAll { job ->
    job.name =~ /^((?!seed-job).)*$/
}

matchedJobs.each { job ->
    println job.name
    job.delete()
}

folder('gitlab') { }

def gitlab_projects = [
  "jenkins",
  "ansible"
]

gitlab_projects.each {
  service = it
  println("${service}")

  pipelineJob("gitlab/${service}") {
    logRotator {
        numToKeep(10)
    }
    definition {
      cpsScm {
      scm {
        git {
          remote {
                url("https://gitlab.com/vgusachenko/${service}.git")
                branch("main")
          }
        }
      }
      scriptPath("Jenkinsfile")
      }
    }
 }

}